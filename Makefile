all: hashtagcounter.class
%.class: %.java
	javac -d . -classpath . $<
clean:
	rm -f *.class
