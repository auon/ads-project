import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Set;
import java.util.Stack;

//Class that have main method
public class hashtagcounter {
	FibonacciHeap fh;
	// Class to define node structure of Fibonacci Heap
	class Node{
		String hashtag;
		int data;
		int degree;
		boolean childcut;
		Node parent;
		Node child;
		Node left;
		Node right;
		//Constructor to initialize a node
		Node(String hashtag,int data){
			this.hashtag = hashtag;
			this.data = data;
			parent = null;
			child = null;
			left = null;
			right = null;
			degree = 0;
			childcut = false;
		}
	}
	// Class that defines Fibonacci Heap
	public class FibonacciHeap{
		// 'max' will point to the max element in heap. This would obviously lie in top level circular doubly linked list.
		Node max;
		Hashtable<String, Node> ht; // Hashtable to keep track of added nodes.
		//Constructor to initialize Fibonacci Heap
		public FibonacciHeap() {
			max = null;
			ht = new Hashtable<>();
		}
		// The new node(which essentially is a new tree) is always added(or melded) to the right of 'max' node.
		public void meld(Node newNode){
			newNode.left = max;
			newNode.right = max.right;
			max.right.left = newNode;
			max.right = newNode;
			// Max-pointer update
			if(newNode.data > max.data){
				max = newNode;
			}
		}
		//remove-Max implementation
		public Node removeMax(){
			// To check if 'max' is the only node in heap. If it is, heap becomes empty and this 'max' is returned.
			if((max.left == max || max.right == max) && max.child == null){
				Node maxremoved = new Node(max.hashtag, max.data);
				max = null;
				return maxremoved;
			}
			//To check if max has sub-trees. If it has, they will be added to level-1 C DLL(Circular Doubly Linked List).
			if (max.child != null){
				//Combine two C DLL
				Node temp = max.child;
				Node t = temp.right;
				while(t != temp){
					t.parent = null;
					t.childcut = false;
					t=t.right;
				}
				temp.left.right = max.right;
				max.right.left = temp.left;
				max.right = temp;
				temp.left = max;
				temp.parent = null;
				temp.childcut = false;
				max.child = null;
			}
			//Remove-max from C D-LL and store it temporarily
			Node maxremoved = new Node(max.hashtag, max.data);
			max.left.right=max.right;
			max.right.left=max.left;
			Node start = max.right; //From 'start' node, we'll start the pairwise combine
			max.left = max;
			max.right = max;
			pairwiseCombine(start);
			return maxremoved;
		}
		//pairwise Combine implementation
		public void pairwiseCombine(Node start){
			//RankTable will point to the trees, hence 'value type' is 'Node'. Its size can go up to number of nodes in heap.
			int rankTableSize = ht.size();
			Hashtable<Integer, Node> rankTable = new Hashtable<>();
			// A temporary stack to hold collection of all trees just before pairwise combine starts. 
			//Its necessary as 'left' and 'right' pointers of the root of these trees will change as the pairwise combine will proceed. 
			Stack<Node> stack = new Stack<>();
			stack.push(start);
			Node temp = start.right;
			while (temp != start){
				stack.push(temp);
				temp=temp.right;
			}
			for(int i=0;i<rankTableSize;i++){
				Node node = new Node("#",-1);
				rankTable.put(i, node);
			}
			while(stack.size()!=0){
				//Pop top node and check if node with similar degree exists in rankTable
				Node s = stack.pop();
				s.left = null;
				s.right = null;
				s.parent = null;
				Node t = rankTable.get(s.degree); 
				//Check if rankTable have some node with degree = s.degree
				if (t.data == -1){
					rankTable.put(s.degree, s); // It didn't. So added the node in rankTable
				}
				else{
					rankTable.remove(s.degree);
					rankTable.put(s.degree, new Node("#",-1));
					if (s.data >= t.data){ // rankTable had this node. So pairwise combine.
						// Add t as child of s
						t.childcut = false;
						if(s.child == null){
							s.child = t;
							t.left = t;
							t.right = t;
						}
						else{
							t.left = s.child.left;
							t.right = s.child;
							s.child.left.right = t;
							s.child.left = t;
							//update child pointer of s
							s.child = t;
						}
						//update parent pointer of t
						t.parent = s;
						s.degree += 1;
						stack.push(s);
					}
					else{
						//Add s as child of t
						s.childcut = false;
						if(t.child == null){
							t.child = s;
							s.left = s;
							s.right = s;
						}
						else{
							s.left = t.child.left;
							s.right = t.child;
							t.child.left.right = s;
							t.child.left = s;
							//update child pointer of t
							t.child = s;
						}
							//update parent pointer of s
						s.parent = t;
						t.degree += 1;
						stack.push(t);
					}
				}
			}
			//Finally join all the final trees in rankTable after pairwise combine using Circular Doubly Linked List. 
			//The 1st max-tree in collection is initialized as 'max'
			// The rest of the trees in rankTable are melded using standard meld algorithm
			boolean maxflag = false;
			Set<Integer> keys = rankTable.keySet();
			for(int key: keys){
				Node t = rankTable.get(key);
				if(t.data != -1){
					if(maxflag == false){
						max = t;
						max.left = max;
						max.right = max;
						max.parent = null;
						maxflag = true;
					}
					else{
						meld(t);
					}
				}
			}	
		}
		//The function that removes a node from its sibling's C DLL
		public void cut(Node t){
			t.left.right = t.right;
			t.right.left = t.left;
		}
		// Standard Cascade Cut algorithm of Fibonacci Heap
		public void cascadeCut(Node t){
			t.childcut = false;
			cut(t); // The node is cut
			Node tparent = t.parent;
			tparent.degree -= 1; // the degree of parent of lost child is decreased by one
			//Check if tparent has other children or not
			if(tparent.degree == 0){
				tparent.child = null;
			}
			else{ // tparent has other children. If t is child pointer of tparent, change pointer to left node of t. Otherwise don't change.
				if(tparent.child == t){
					tparent.child = t.left;
				}
			}
			// Now adding the cut out child to top level C DLL
			t.parent = null;
			t.left = t;
			t.right = t;
			meld(t);
			// From here onwards, Cascade cut above the level of cut out child starts, if required.
			if(tparent.parent != null){
				if(tparent.childcut == true){
					cascadeCut(tparent);
				}
				else{
					tparent.childcut = true;
				}
			}
		}
		// Standard Increase-Key implementation of Fibonacci Heap
		public void increaseKey(String hashtag, int incAmt){
			Node temp = ht.get(hashtag);
			temp.data += incAmt;
			//Check if Max-Heap property is violated
			if (temp.parent == null){
				// Its on top level C D-LL. Check if this element has become bigger than max.
				if(temp.data > max.data){
					max = temp;
				}
				return;
				//Done!
			}
			if (temp.parent.data < temp.data){
				cascadeCut(temp);
			}
		}
		// Insert Algorithm of Fibonacci Heap
		public void insert(String hashtag, int data){
			//New node created
			Node newNode = new Node(hashtag, data);
			// If Heap doesn't exist, make first max-tree with this newNode.
			if(max == null){
				max = newNode;
				max.left = max;
				max.right = max;
			}
			else{
				// Meld newly created tree having NewNode with rest of the heap
				meld(newNode);
			}
			// update hashtable
			ht.put(hashtag, newNode);
		}
		
	}
	//Constructor to initialize Fibonacci Heap
	public hashtagcounter() {
		fh = new FibonacciHeap();
	}
	// Function to check if hashtag exists in hashtable for fast lookups (as required by Fibonacci Heap)
	public boolean hashtagExists(String hashtag){
		if(fh.ht.containsKey(hashtag)){
			return true;
		}
		else{
			return false;
		}
	}
	
	public static void main(String[] args){
		hashtagcounter htc = new hashtagcounter();
		BufferedReader br = null;
		PrintWriter writer = null;
		try {

			String sCurrentLine;
			br = new BufferedReader(new FileReader(args[0]));
			writer = new PrintWriter("output_file.txt");
			while ((sCurrentLine = br.readLine()) != null) {
				//Case1: If "stop" appears in the input file, stop the execution
				if(sCurrentLine.trim().toLowerCase().equals("stop")){
					//System.out.println("Completed");
					break;
				}
				//Case2: If it is an insert or increase key operation
				if(sCurrentLine.contains(" ")){
					sCurrentLine = sCurrentLine.trim();
					String[] arr = sCurrentLine.split(" ");
					String hashtag = arr[0].substring(1);
					int data = Integer.parseInt(arr[1]);
					if(htc.hashtagExists(hashtag)){
						htc.fh.increaseKey(hashtag, data); //If hashtag exist, increase key
					}
					else{
						htc.fh.insert(hashtag,data);	   //If hashtag doesn't exist, insert
					}
				}
				//Case3: If it is a 'query' operation. A number of remove-maxes will be performed.
				else{
					int query = Integer.parseInt(sCurrentLine.trim());
					// A List to hold the remove maxes
					LinkedList<Node> removedmaxes = new LinkedList<>();
					// A List to hold the output
					LinkedList<String> output = new LinkedList<>();
					while (query > 0){
						Node temp = htc.fh.removeMax(); //Remove-max
						htc.fh.ht.remove(temp.hashtag); //Update ht hashtable and remove the entry for this node
						output.add(temp.hashtag);
						removedmaxes.add(temp);
						query--;
					}
					// Now add back removed-maxes as required for project scope. 
					//We know the first max removed was the overall max node. 
					//So we add that first as node changes made for updating max would be minimal and will happen once. 
					while(removedmaxes.size()>0){
						Node rm = removedmaxes.removeFirst();
						htc.fh.insert(rm.hashtag, rm.data);
					}
					//Writing output to output file
					String toWrite = "";
					while (output.size() > 0){
						toWrite += output.removeFirst()+",";
					}
					toWrite = toWrite.substring(0, toWrite.length()-1);
					writer.println(toWrite);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
				writer.flush();
				writer.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
